import { rainbow } from './color'
import { style } from './style'

interface IButtonable {
  action(): void
  fraction: number
  title: string
}

const scroll = document.createElement('div')
document.body.appendChild(scroll)

export const container = document.createElement('div')
scroll.appendChild(container)

container.style.backgroundImage = `linear-gradient(to bottom, ${'1234567890ab'
  .split('')
  .map((x, i) => rainbow(i / 11))
  .join(', ')})`

let buttonCount = 0

export const button = ({ title, action, fraction }: IButtonable) => {
  buttonCount++
  const element = document.createElement('button')
  style(element, {
    backgroundColor: buttonCount % 2 ? '#999' : '#777',
    textColor: buttonCount % 2 ? '#444' : '#eee'
  })
  element.innerHTML = title
  element.addEventListener('click', () => {
    container.style.backgroundColor = rainbow(fraction)
    action()
  })
  container.appendChild(element)
}
