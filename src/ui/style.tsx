interface IStyleProperties {
  backgroundColor?: string
  textColor?: string
}

export const style = (element: HTMLElement, properties: IStyleProperties) => {
  if (properties.backgroundColor) {
    element.style.backgroundColor = properties.backgroundColor
  }
  if (properties.textColor) {
    element.style.color = properties.textColor
  }
}
