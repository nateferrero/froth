export const hue = (angle: number) =>
  `hsl(${angle}, 100%, ${angle > 240 ? 20 + (270 - angle) : 50}%)`

export const flex = (x: number) =>
  0.2 * x + 0.8 * (x < 0.5 ? 2 * x * x : 1 - 2 * (1 - x) * (1 - x))

export const rainbow = (fraction: number) => hue(270 * flex(fraction))
