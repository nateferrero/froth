export class wave {
  audioContext = new AudioContext()
  source: OscillatorNode

  constructor() {
    this.audioContext.suspend()

    const source = this.audioContext.createOscillator()
    source.type = 'square'
    source.frequency.setValueAtTime(100, this.audioContext.currentTime)
    source.connect(this.audioContext.destination)
    source.start()
    this.source = source
  }

  start() {
    this.audioContext.resume()
  }

  stop() {
    this.audioContext.suspend()
  }

  frequency(freq: number) {
    this.source.frequency.exponentialRampToValueAtTime(
      freq,
      this.audioContext.currentTime + 0.001
    )
    this.source.frequency.exponentialRampToValueAtTime(
      100,
      this.audioContext.currentTime + 0.02
    )
  }
}
