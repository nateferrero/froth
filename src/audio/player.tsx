import { sound } from '../concepts/sound'

import { wave } from './wave'

export class player {
  context = new AudioContext()
  waves = [1, 2, 3, 4].map(x => new wave())

  play(sound: sound) {
    console.log('playing', sound)
    this.waves.forEach(wave => (Math.random() > 0.5 ? wave.start() : null))
    this.waves.forEach(wave => wave.frequency(100 + 100 * Math.random()))
    ;(window as any).x = this
  }
}
