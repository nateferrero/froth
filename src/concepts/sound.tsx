export class sound {
  public readonly title: string

  constructor(title: string) {
    this.title = title
  }

  action() {
    console.log('playing')
  }
}
