import { track_0_targeted_dive } from './tracks/track-0-targeted-dive'
import { track_1_contaminant_isolation } from './tracks/track-1-contaminant-isolation'
import { track_2_star_construction } from './tracks/track-2-star-construction'
import { track_3_reverence } from './tracks/track-3-reverence'
import { track_4_salvation } from './tracks/track-4-salvation'
import { track_5_forest } from './tracks/track-5-forest'
import { track_6_interdimensional_transparency } from './tracks/track-6-interdimensional-transparency'
import { track_7_invincible_admiration } from './tracks/track-7-invincible-admiration'
import { track_8_cosmic_bondage } from './tracks/track-8-cosmic-bondage'
import { track_9_free_all_souls } from './tracks/track-9-free-all-souls'
import { track_a_revelation_ecstasy } from './tracks/track-a-revelation-ecstasy'
import { track_b_froth } from './tracks/track-b-froth'

import { player } from './audio/player'
import { button } from './ui/button'

const tracks = [
  track_0_targeted_dive,
  track_1_contaminant_isolation,
  track_2_star_construction,
  track_3_reverence,
  track_4_salvation,
  track_5_forest,
  track_6_interdimensional_transparency,
  track_7_invincible_admiration,
  track_8_cosmic_bondage,
  track_9_free_all_souls,
  track_a_revelation_ecstasy,
  track_b_froth
]

const device = new player()

button({
  action() {},
  fraction: 1,
  title: 'Nate Ferrero'
})

button({
  action() {},
  fraction: 1,
  title: 'froth'
})

const fancy = (x: number) =>
  [
    Array(Math.floor(x / 5))
      .fill(null)
      .map(y => '|')
      .join(''),
    Array(x % 5)
      .fill(null)
      .map(y => '•')
      .join('')
  ].join('')

tracks.forEach((track, index) =>
  button({
    action: () => device.play(track),
    fraction: index / (tracks.length - 1),
    title: `<span>${fancy(index + 1)}</span> ${track.title}`
  })
)
