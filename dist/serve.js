const http = require('http')
const fs = require('fs')
const path = require('path')

const PORT = 8421

http
  .createServer((request, response) => {
    console.log('request ', request.url)

    let filePath = '.' + request.url
    if (filePath == './') {
      filePath = './index.html'
    } else if (!filePath.endsWith('.js')) {
      filePath += '.js'
    }

    const extname = String(path.extname(filePath)).toLowerCase()
    const mimeTypes = {
      '.html': 'text/html',
      '.js': 'text/javascript',
      '.css': 'text/css',
      '.json': 'application/json',
      '.png': 'image/png',
      '.svg': 'image/svg+xml',
      '.wav': 'audio/wav'
    }

    const contentType = mimeTypes[extname] || 'application/octet-stream'

    fs.readFile(path.join(__dirname, filePath), (error, content) => {
      if (error) {
        response.writeHead(500)
        response.end(`Error ${error.code}`)
      } else {
        response.writeHead(200, { 'Content-Type': contentType })
        response.end(content, 'utf-8')
      }
    })
  })
  .listen(PORT)

console.log(`Server running at http://localhost:${PORT}/`)
